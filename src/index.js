"use strict";
require("dotenv").config();
const express = require("express");
const helmet = require("helmet");
const morgan = require("morgan");
const compression = require("compression");
const fileUpload = require("express-fileupload");
const app = express("morgan");

//init middlerWare
app.use(morgan("dev"));
app.use(helmet());
app.use(compression());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// use form-data
app.use(
  fileUpload({
    limits: { fileSize: 10 * 1024 * 1024 },
    useTempFiles: true,
    tempFileDir: "/tmp/",
  })
);

//init dbs
require("./database/init.database");

//init routes
app.get("/", function (req, res, next) {
  return res.status(200).json({
    message: "hello",
  });
});
app.use("/", require("./routes"));
//handler error
app.use((req, res, next) => {
  const error = new Error("Not Found");
  error.status = 404;
  next(error);
});

app.use((err, req, res, next) => {
  const status = err.status || 500;
  return res.status(status).json({
    status: "error",
    code: status,
    message: err.message || "Internal Server Error",
  });
});

module.exports = app;
