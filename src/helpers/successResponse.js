"use strict";

class SuccessResponse {
  constructor({ message = "", status = 200, data = {} }) {
    this.message = message;
    this.status = status;
    this.data = data;
  }

  send(res) {
    return res.status(+this.status).json(this);
  }
}

module.exports = SuccessResponse;
