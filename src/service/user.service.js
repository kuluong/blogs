"use strict";

const ErrorResponse = require("../helpers/errorResponse");
const bcrypt = require("bcrypt");
const userModel = require("../modules/model/user.model");
const { findUser } = require("../modules/repositories/user.repo");
const { createTokenPair } = require("../auth/auth");
const { getInfoData } = require("../utils/getInfoData");

class UserService {
  static register = async (payload) => {
    const {
      username,
      password,
      firstName = "",
      lastName = "",
      email = "",
      age = "",
      address = "",
    } = payload;

    if (!username || !password) {
      throw new ErrorResponse("username or password not empty", 400);
    }

    //check username exists
    const foundUser = await findUser(username);
    if (foundUser) throw new ErrorResponse("username exists", 403);
    //hash a password
    const passwordHash = await bcrypt.hash(password, 10);

    const createUser = await userModel.create({
      username,
      password: passwordHash,
      firstName,
      lastName,
      email,
      age,
      address,
    });
    if (!createUser) {
      throw new ErrorResponse("Register user fails", 400);
    }
    const token = createTokenPair(payload, process.env.KEY_SECRET);
    return {
      token,
    };
  };
  static login = async (payload) => {
    const { username, password } = payload;
    if (!username || !password) {
      throw new ErrorResponse("username or password not empty", 400);
    }

    //check user exists

    const foundUser = await findUser(username);
    if (!foundUser) {
      throw new ErrorResponse("username not register");
    }

    //compare password

    const comparePass = await bcrypt.compare(password.toString(), foundUser.password);
    if (!comparePass) {
      throw new ErrorResponse("authentication fail", 401);
    }

    //neu dang nhap thanh cong thi gui lai token
    const token = createTokenPair({ username }, process.env.KEY_SECRET);

    return {
      data: getInfoData({
        fields: ["username", "firstName", "lastName", "email", "age", "address"],
        object: foundUser,
      }),
      token,
    };
  };
  static info = async (token) => {};
  static updateUser = async (payload) => {};
  static lockUser = async (payload) => {};
  static unLockUser = async (payload) => {};
}

module.exports = UserService;
