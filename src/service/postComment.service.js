"use strict";

const ErrorResponse = require("../helpers/errorResponse");
const postModel = require("../modules/model/post.model");
const postCommentModel = require("../modules/model/postComment.model");
const { options } = require("../routes/postComment");

class PostCommentService {
  static newPostCommentByUser = async ({ postId, userId, comment }) => {
    if (!postId) {
      throw new ErrorResponse("there are not posted", 400);
    }
    if (!userId) {
      throw new ErrorResponse("user not exists", 400);
    }
    const foundPost = await postModel.findById(postId).lean();
    if (!foundPost) {
      throw new ErrorResponse("post not exists");
    }
    const newPostComment = await postCommentModel.create({
      idPost: postId,
      username: userId,
      comment: comment,
    });
    if (!newPostComment) {
      throw new ErrorResponse("comment fail", 400);
    }
    return newPostComment;
  };
  static getAllCommentOfPost = async ({ postId }) => {
    if (!postId) {
      throw new ErrorResponse("there are no posted", 400);
    }
    const foundPost = await postModel.findById(postId);
    if (!foundPost) {
      throw new ErrorResponse("post not exists!!");
    }
    const listCommentOfPost = await postCommentModel
      .find({ idPost: postId })
      .populate("idPost");

    if (!listCommentOfPost || listCommentOfPost.length < 1) {
      return {
        postId: foundPost._id,
        author: foundPost.username,
        content: foundPost.content,
        comments: [],
        createdAt: new Date(foundPost.createdAt).toLocaleString(),
        updatedAt: new Date(foundPost.updatedAt).toLocaleString(),
      };
    }

    const post = {
      postId: listCommentOfPost[0].idPost._id,
      author: listCommentOfPost[0].idPost.username,
      content: listCommentOfPost[0].idPost.content,
      createdAt: new Date(listCommentOfPost[0].idPost.createdAt).toLocaleString(),
      updatedAt: new Date(listCommentOfPost[0].idPost.updatedAt).toLocaleString(),
      comments: listCommentOfPost.map((comment) => {
        return {
          idCommentByUse: comment._id,
          userComment: comment.username,
          contentComment: comment.comment,
          createdAt: new Date(comment.createdAt).toLocaleString(),
          updatedAt: new Date(comment.updatedAt).toLocaleString(),
        };
      }),
    };

    return post;
  };
}
module.exports = PostCommentService;
