"use strict";

const ErrorResponse = require("../helpers/errorResponse");
const postModel = require("../modules/model/post.model");
const postCommentModel = require("../modules/model/postComment.model");
const {
  listPostByUser,
  listPost,
} = require("../modules/repositories/post.repo");

class PostService {
  static newPostByUser = async ({ userId, content }) => {
    if (!userId) {
      throw new ErrorResponse("user not found", 400);
    }
    if (!content) {
      throw new ErrorResponse("content not empty", 400);
    }
    //create post

    const newPost = await postModel.create({
      username: userId,
      content,
    });
    if (!newPost) {
      throw new ErrorResponse("create new post fail", 400);
    }
    return newPost;
  };

  static listPostByUser = async ({ userId, limit = 50, page = 1 }) => {
    if (!userId) {
      throw new ErrorResponse("user not found", 400);
    }
    const listPost = await listPostByUser(userId, limit, page);
    if (!listPost) {
      throw new ErrorResponse("There are no posts created by user");
    }
    return {
      length: listPost.length,
      data: listPost,
    };
  };

  static listPost = async ({ limit = 50, page = 1 }) => {
    const listPosts = await listPost(limit, page);

    if (!listPosts) {
      throw new ErrorResponse("There are no posts created");
    }
    console.log("here");
    let posts = [];

    for (let index = 0; index <= listPosts.length - 1; index++) {
      const post = listPosts[index];

      const listCommentOfPost = await postCommentModel
        .find({ idPost: listPosts[index]._id })
        .populate("idPost")
        .lean();

      if (!listCommentOfPost || listCommentOfPost?.length < 1) {
        posts.push({
          postId: post._id,
          author: post.username,
          content: post.content,
          comments: [],
          totalComment: 0,
          createdAt: new Date(post.createdAt).toLocaleString(),
          updatedAt: new Date(post.updatedAt).toLocaleString(),
        });
      } else {
        posts.push({
          postId: listCommentOfPost[0].idPost._id,
          author: listCommentOfPost[0].idPost.username,
          content: listCommentOfPost[0].idPost.content,
          createdAt: new Date(
            listCommentOfPost[0].idPost.createdAt
          ).toLocaleString(),
          updatedAt: new Date(
            listCommentOfPost[0].idPost.updatedAt
          ).toLocaleString(),
          totalComment: listCommentOfPost.length,
          comments: listCommentOfPost.map((comment) => {
            return {
              idCommentByUse: comment._id,
              userComment: comment.username,
              contentComment: comment.comment,
              createdAt: new Date(comment.createdAt).toLocaleString(),
              updatedAt: new Date(comment.updatedAt).toLocaleString(),
            };
          }),
        });
      }
    }

    return {
      length: listPosts.length,
      data: posts,
    };
  };
  static updatePostByAuthor = async ({ idPost, author, content }) => {
    if (!idPost) {
      throw new ErrorResponse("post not exists", 400);
    }
    if (!author) {
      throw new ErrorResponse("author not exists!", 400);
    }
    //find post by postId and compare author of post with author has the same
    const foundPost = await postModel.findById(idPost).lean();
    if (!foundPost) {
      throw new ErrorResponse("post not exists", 400);
    }

    if (foundPost.username !== author) {
      throw new ErrorResponse(
        `you don't have permission to edit this post `,
        400
      );
    }
    //if match author, and postId valid, we will update it
    console.log(content);
    const updatePost = await postModel.findByIdAndUpdate(
      idPost,
      { content },
      {
        upsert: true,
        new: true,
      }
    );
    if (!updatePost) {
      throw new ErrorResponse("update fail", 400);
    }
    return updatePost;
  };
  static hidePostByAuthor = async ({ author, idPost }) => {
    if (!idPost) {
      throw new ErrorResponse("post not exists", 400);
    }
    if (!author) {
      throw new ErrorResponse("author not exists!", 400);
    }
    //find post by postId and compare author of post with author has the same
    const foundPost = await postModel.findById(idPost).lean();
    if (!foundPost) {
      throw new ErrorResponse("post not exists", 400);
    }

    if (foundPost.username !== author) {
      throw new ErrorResponse(
        `you don't have permission to edit this post `,
        400
      );
    }
    foundPost.status = false;
    const { modifiedCount } = await postModel.updateOne(
      { _id: idPost },
      { status: false },
      { upsert: true, new: true }
    );
    return modifiedCount;
  };
  static restorePostByAuthor = async ({ author, idPost }) => {
    if (!idPost) {
      throw new ErrorResponse("post not exists", 400);
    }
    if (!author) {
      throw new ErrorResponse("author not exists!", 400);
    }
    //find post by postId and compare author of post with author has the same
    const foundPost = await postModel.findById(idPost).lean();
    if (!foundPost) {
      throw new ErrorResponse("post not exists", 400);
    }

    if (foundPost.username !== author) {
      throw new ErrorResponse(
        `you don't have permission to edit this post `,
        400
      );
    }
    foundPost.status = false;
    const { modifiedCount } = await postModel.updateOne(
      { _id: idPost },
      { status: true },
      { upsert: true, new: true }
    );
    return modifiedCount;
  };
}

module.exports = PostService;
