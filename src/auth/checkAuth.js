"use strict";

const ErrorResponse = require("../helpers/errorResponse");
const { verifyToken } = require("./auth");

const checkAuth = (req, res, next) => {
  // check headers has token

  const headerAccessToken = req.headers.authorization;

  if (!headerAccessToken || !headerAccessToken.startsWith("Bearer")) {
    throw new ErrorResponse("login fail, please login again", 400);
  }

  const token = headerAccessToken.split(" ")[1];

  if (!token) {
    throw new ErrorResponse("login fail, please login again");
  }

  //verify token

  const decode = verifyToken(token, process.env.KEY_SECRET);
  if (decode) {
    req.user = decode.username;
  }
  next();
};

module.exports = checkAuth;
