"use strict";
const JWT = require("jsonwebtoken");

const createTokenPair = (payload, keySecret) => {
  const token = JWT.sign(payload, keySecret, {
    expiresIn: 60 * 60 * 60, // 360 hour
    // expiresIn: 60, // 60 seconds
  });
  return token;
};

const verifyToken = (token, keySecret) => {
  return JWT.verify(token, keySecret);
};

module.exports = {
  createTokenPair,
  verifyToken,
};
