"use strict";

const express = require("express");
const asyncHandler = require("../../helpers/asyncHandler");
const PostCommentController = require("../../controller/postComment.controller");

const router = express.Router();

router.post("/post-comment", asyncHandler(PostCommentController.postComment));
router.get(
  "/all-comment-of-post/:postId",
  asyncHandler(PostCommentController.getAllCommentOfPost)
);

module.exports = router;
