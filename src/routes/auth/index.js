"use strict";

const express = require("express");
const asyncHandler = require("../../helpers/asyncHandler");
const AuthController = require("../../controller/authen.controller");

const router = express.Router();

router.post("/auth/register", asyncHandler(AuthController.register));
router.post("/auth/login", asyncHandler(AuthController.login));

module.exports = router;
