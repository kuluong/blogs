"use strict";
const express = require("express");
const asyncHandler = require("../../helpers/asyncHandler");
const PostController = require("../../controller/post.controller");

const router = express.Router();

router.post("/post", asyncHandler(PostController.newPost));
router.get("/post", asyncHandler(PostController.listPost));
router.get("/post/list-post-by-user", asyncHandler(PostController.listPostByUser));
router.put("/post", asyncHandler(PostController.updatePostByAuthor));
router.post("/post/hide-post/:idPost", asyncHandler(PostController.hidePostByAuthor));
router.post(
  "/post/restore-post/:idPost",
  asyncHandler(PostController.restorePostByAuthor)
);

module.exports = router;
