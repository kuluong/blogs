"use strict";

const express = require("express");
const authentication = require("../auth/checkAuth");
const router = express.Router();

//auth
router.use("/v1/api/", require("./auth"));

//authentication middleware
router.use(authentication);

//post
router.use("/v1/api/", require("./post"));
router.use("/v1/api/comments", require("./postComment"));

module.exports = router;
