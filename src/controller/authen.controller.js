"use strict";

const successResponse = require("../helpers/successResponse");
const UserService = require("../service/user.service");

class AuthController {
  static register = async (req, res, next) => {
    return new successResponse({
      message: "register user success",
      status: 201,
      data: await UserService.register(req.body),
    }).send(res);
  };
  static login = async (req, res, next) => {
    return new successResponse({
      message: "login success",
      status: 200,
      data: await UserService.login(req.body),
    }).send(res);
  };
}
module.exports = AuthController;
