"use strict";

const SuccessResponse = require("../helpers/successResponse");
const PostCommentService = require("../service/postComment.service");

class PostCommentController {
  static postComment = async (req, res, next) => {
    return new SuccessResponse({
      message: "comment success ",
      status: "200",
      data: await PostCommentService.newPostCommentByUser({
        postId: req.body.postId,
        userId: req.user,
        comment: req.body.comment,
      }),
    }).send(res);
  };
  static getAllCommentOfPost = async (req, res, next) => {
    return new SuccessResponse({
      message: "comment success ",
      status: "200",
      data: await PostCommentService.getAllCommentOfPost({
        postId: req.params.postId,
      }),
    }).send(res);
  };
}
module.exports = PostCommentController;
