"use strict";

const SuccessResponse = require("../helpers/successResponse");
const PostService = require("../service/post.service");

class PostController {
  static newPost = async (req, res, next) => {
    return new SuccessResponse({
      message: "new post success",
      status: 201,
      data: await PostService.newPostByUser({
        userId: req.user,
        content: req.body.content,
      }),
    }).send(res);
  };
  static listPostByUser = async (req, res, next) => {
    return new SuccessResponse({
      message: "get post by user success",
      status: 200,
      data: await PostService.listPostByUser({
        userId: req.user,
      }),
    }).send(res);
  };
  static listPost = async (req, res, next) => {
    return new SuccessResponse({
      message: "get post by user success",
      status: 200,
      data: await PostService.listPost({}),
    }).send(res);
  };
  static updatePostByAuthor = async (req, res, next) => {
    return new SuccessResponse({
      message: "update post success",
      status: 200,
      data: await PostService.updatePostByAuthor({
        idPost: req.body.idPost,
        author: req.body.username,
        content: req.body.content,
      }),
    }).send(res);
  };
  static hidePostByAuthor = async (req, res, next) => {
    return new SuccessResponse({
      message: "Hide post success",
      status: 200,
      data: await PostService.hidePostByAuthor({
        idPost: req.params.idPost,
        author: req.user,
      }),
    }).send(res);
  };
  static restorePostByAuthor = async (req, res, next) => {
    return new SuccessResponse({
      message: "restore post success",
      status: 200,
      data: await PostService.restorePostByAuthor({
        idPost: req.params.idPost,
        author: req.user,
      }),
    }).send(res);
  };
}

module.exports = PostController;
