"use strict";

const userModel = require("../model/user.model");

const findUser = async (username) => {
  return await userModel.findOne({ username }).lean();
};

module.exports = {
  findUser,
};
