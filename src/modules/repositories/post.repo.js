"use strict";

const postModel = require("../model/post.model");

const listPost = (limit, page, status = true) => {
  const skip = (page - 1) * limit;
  return postModel
    .find({ status })
    .sort({ _id: 1 })
    .skip(skip)
    .limit(limit)
    .select(["_id", "username", "content", "listComment", "createdAt", "updatedAt"]);
};

const listPostByUser = async (userId, status = true, limit = 50, page = 1) => {
  const skip = (page - 1) * limit;
  return await postModel
    .find({ username: userId, status: status })
    .sort({ _id: 1 })
    .skip(skip)
    .limit(limit)
    .select(["_id", "username", "content", "listComment", "createdAt", "updatedAt"])
    .lean();
};

module.exports = {
  listPost,
  listPostByUser,
};
