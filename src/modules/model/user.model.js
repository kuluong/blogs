"use strict";

const mongoose = require("mongoose");

const DOCUMENT_NAME = "user";
const COLLECTION_NAME = "users";

const userSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      require: [true, "username not empty"],
      unique: true,
      min: [4, "username must not be shorter than 4 characters "],
      max: [20, "username must not be long than 20 characters "],
    },
    password: {
      type: String,
      require: [true, "password not empty"],
      min: [4, "password must not be shorter than 4 characters "],
      max: [20, "password must not be long than 20 characters "],
    },
    firstName: String,
    lastName: String,
    email: String,
    age: Number,
    address: String,
    status: {
      type: Boolean,
      default: true,
    },
  },
  {
    timestamps: true,
    collection: COLLECTION_NAME,
  }
);

module.exports = mongoose.model(DOCUMENT_NAME, userSchema);
