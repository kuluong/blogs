"use strict";

const mongoose = require("mongoose");

const DOCUMENT_NAME = "post";
const COLLECTION_NAME = "posts";

const postSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      ref: "user",
    },
    content: {
      type: String,
      max: [256, "content must not be long than 256 characters "],
    },
    status: {
      type: Boolean,
      default: true,
    },
    // listComment: {
    //   type: Array,
    //   default: [],
    //   ref: "postComment",
    // },
  },
  {
    timestamps: true,
    collection: COLLECTION_NAME,
  }
);

module.exports = mongoose.model(DOCUMENT_NAME, postSchema);
