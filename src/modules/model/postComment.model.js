"use strict";

const mongoose = require("mongoose");
const { model, Schema } = require("mongoose");
const DOCUMENT_NAME = "postComment";
const COLLECTION_NAME = "postComment";

const postCommentSchema = new Schema(
  {
    idPost: {
      type: Schema.Types.ObjectId,
      ref: "post",
    },
    username: {
      type: String,
      ref: "user",
    },
    comment: {
      type: String,
    },
  },
  {
    timestamps: true,
    collection: COLLECTION_NAME,
  }
);
module.exports = model(DOCUMENT_NAME, postCommentSchema);
