"use strict";
const mongoose = require("mongoose");
const connStr = `mongodb+srv://${process.env.USERNAME_DB}:${process.env.PASSWORD_DB}@blogs.tetfdac.mongodb.net/`;
class Database {
  constructor() {
    this.connect();
  }
  connect() {
    mongoose
      .connect(connStr)
      .then((res) => {
        console.log("connect to database success");
      })
      .catch((error) => console.log(`connect to database fail :: ${error}`));
  }

  static getInstance() {
    if (!Database.instance) {
      Database.instance = new Database();
    }
    return Database.instance;
  }
}

module.exports = Database.getInstance();
