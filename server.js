"use strict";
2
const app = require("./src/index");

const PORT = process.env.PORT || 3036;
app.listen(PORT, () => {
  console.log(`server running with port = ${PORT}`);
});
